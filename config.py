import os
from pathlib import Path

from app.utils import parse_cities


class BaseConfig:
    BASE_DIR = Path(__file__).parent
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = \
        os.environ.get('DATABASE_URL',
                       'sqlite:///' + os.path.join(BASE_DIR, 'data.sqlite'))
    AIR_QUALITY_API_TOKEN = "c4b60702aad3209fbd7fec9e88b0bb6b31dd8cc0"
    AIR_QUALITY_API_URL = "https://api.waqi.info/feed/"
    TRACKED_CITIES = parse_cities("cities.json")
    PM25_API_LOCATION = "data", "iaqi", "pm25", "v"
    PM10_API_LOCATION = "data", "iaqi", "pm10", "v"
    TIMESTAMP_API_LOCATION = "data", "time", "v"


class DevelopmentConfig(BaseConfig):
    DEBUG = True


class ProductionConfig(BaseConfig):
    DEBUG = False


config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
}
