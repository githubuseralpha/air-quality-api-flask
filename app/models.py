from sqlalchemy import ForeignKey, sql
from . import db

MAX_NAME_LEN = 50


class Country(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(MAX_NAME_LEN))
    cities = db.relationship('City', back_populates='country', lazy=False)

    def __repr__(self):
        return f"City (id={self.id}, name={self.name})"

    @property
    def serialize(self):
        return {
           'id': self.id,
           'name': self.name,
           'cities': [item.serialize for item in self.cities],
        }


class City(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(MAX_NAME_LEN))
    country_id = db.Column(db.Integer, ForeignKey('country.id'))
    country = db.relationship('Country', back_populates="cities", lazy=False)
    pm25 = db.relationship('Pm25Record', backref='city',
                           lazy=False, order_by='Pm25Record.date')
    pm10 = db.relationship('Pm10Record', backref='city',
                           lazy=False, order_by='Pm10Record.date')

    def __repr__(self):
        return f"City (id={self.id}, name={self.name})"

    @staticmethod
    def avg(pms):
        pms_filtered = [item for item in pms if item.value is not None]
        if len(pms_filtered) == 0:
            return None
        return sum([item.value for item in pms_filtered]) / len(pms_filtered)

    @property
    def serialize(self):
        return {
           'id': self.id,
           'name': self.name,
           'country_id': self.country_id,
           'country_name': self.country.name,
           'pm10': [item.serialize for item in self.pm10],
           'pm25': [item.serialize for item in self.pm25],
           'pm10_avg': self.avg(self.pm10),
           'pm25_avg': self.avg(self.pm25),
        }


class Record(db.Model):
    __abstract__ = True

    date = db.Column(db.DateTime(timezone=True),
                     server_default=sql.func.now())
    value = db.Column(db.Integer, nullable=True)

    @property
    def serialize(self):
        return {
           'id': self.id,
           'date': self.date.strftime("%Y-%m-%d"),
           'value': self.value
        }


class Pm25Record(Record):
    __table_args__ = (
        db.UniqueConstraint('date', 'city_id', name='unique_pm25_date'),
        )
    id = db.Column(db.Integer, primary_key=True)
    city_id = db.Column(db.Integer, ForeignKey('city.id'))

    def __repr__(self):
        return f"PM25 (value={self.value})"


class Pm10Record(Record):
    __table_args__ = (
        db.UniqueConstraint('date', 'city_id', name='unique_pm10_date'),
        )
    id = db.Column(db.Integer, primary_key=True)
    city_id = db.Column(db.Integer, ForeignKey('city.id'))

    def __repr__(self):
        return f"PM10 (value={self.value})"
