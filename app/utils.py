from dataclasses import dataclass
import json


@dataclass
class CityData:
    official_name: str = None
    api_name: str = None
    country_name: str = None


def parse_cities(path):
    with open(path, "r") as read_file:
        data = json.load(read_file)
    return [CityData(city['officialName'], city['apiName'],
                     city['countryName']) for city in data]


def get_nested(li, *args, default=None):
    try:
        for arg in args:
            li = li[arg]
        return li
    except KeyError:
        return default
