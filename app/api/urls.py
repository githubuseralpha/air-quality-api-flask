import datetime

from flask import jsonify
from sqlalchemy.sql import func

from . import api
from ..models import City, Country, Pm10Record, Pm25Record
from app import db

YEAR_LEN = 365
MONTH_LEN = 30


def jsonify_row(row, keys):
    result = []
    for item in row:
        result.append({
            key: item[i] for i, key in enumerate(keys)
        })
    return jsonify(result)


def get_dates_comp(days):
    def compare_dates(date):
        current_time = datetime.datetime.utcnow()
        x_days_ago = current_time - datetime.timedelta(days=days)
        return date >= x_days_ago
    return compare_dates


def filter_by_time(date, period):
    period_cases = {
        'day': get_dates_comp(1),
        'month': get_dates_comp(MONTH_LEN),
        'year': get_dates_comp(YEAR_LEN),
        'all': lambda _: True
    }
    return period_cases.get(period, lambda _: False)(date)


def query_city_ranking(Pm, period):
    return (db.session.query(City.id, City.name,
                             Country.name,
                             func.round(func.avg(Pm.value), 2))
            .group_by(City.id, Country.name)
            .join(City, City.id == Pm.city_id)
            .join(Country, Country.id == City.country_id)
            .filter(filter_by_time(Pm.date, period))
            .filter(Pm.value.isnot(None))
            .order_by(func.avg(Pm.value).desc()))


def query_country_ranking(Pm, period):
    return (db.session.query(Country.id, Country.name, func.avg(Pm.value))
            .group_by(Country.name, Country.id)
            .join(City, City.country_id == Country.id)
            .join(Pm, Pm.city_id == City.id)
            .filter(filter_by_time(Pm.date, period))
            .filter(Pm.value.isnot(None))
            .order_by(func.avg(Pm.value).desc()))


@api.route('/cities/', methods=['GET'])
def get_cities():
    cities = City.query.order_by(City.id).all()
    return jsonify([city.serialize for city in cities])


@api.route('/cities/<int:city_id>', methods=['GET'])
def get_city(city_id):
    city = (City.query.get(city_id))
    if not city:
        return jsonify({'error': 'No city with such id'}), 404
    return jsonify(city.serialize)


@api.route('/cities/ranking/<string:pm_type>/<string:period>', methods=['GET'])
def get_city_ranking(pm_type, period):
    if pm_type not in ['pm25', 'pm10']:
        return jsonify({'error': 'Wrong pm type'}), 404
    Pm = Pm25Record if pm_type == "pm25" else Pm10Record
    query = query_city_ranking(Pm, period)
    city_ranking = query.all()
    return jsonify_row(city_ranking,
                       ['id', 'name', 'country', f"{pm_type}_value"])


@api.route('/countries/', methods=['GET'])
def get_countries():
    countries = Country.query.order_by(Country.id).all()
    return jsonify([country.serialize for country in countries])


@api.route('/countries/<int:country_id>', methods=['GET'])
def get_country(country_id):
    country = (Country.query.get(country_id))
    if not country:
        return jsonify({'error': 'No country with such id'}), 404
    return jsonify(country.serialize)


@api.route('/countries/ranking/<string:pm_type>/<string:period>',
           methods=['GET'])
def get_country_ranking(pm_type, period):
    if pm_type not in ['pm25', 'pm10']:
        return jsonify({'error': 'Wrong pm type'}), 404
    Pm = Pm25Record if pm_type == "pm25" else Pm10Record
    query = query_country_ranking(Pm, period)
    country_ranking = query.all()
    return jsonify_row(country_ranking,
                       ['id', 'name', f"{pm_type}_value"])
