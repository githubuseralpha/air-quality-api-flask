from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from config import config

db = SQLAlchemy()
migrate = Migrate(db=db, render_as_batch=True)


def create_app(config_name='development'):
    app = Flask(__name__)
    app.config.from_object(config[config_name])

    db.init_app(app)
    migrate.init_app(app, db)

    from .api import api as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='/api/')

    from .jobs import scheduler
    scheduler.init_app(app)
    scheduler.start()

    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    return app
