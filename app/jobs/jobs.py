from datetime import datetime

import requests
from flask import current_app

from app import db
from app.models import City, Pm25Record, Pm10Record
from app.utils import get_nested
from . import scheduler


@scheduler.job
def fetch_from_api():
    for city in current_app.config.get('TRACKED_CITIES'):
        timestamp, pm25_val, pm10_val = \
            get_data(city.api_name)
        if timestamp:
            city_inst, pm25_inst, pm10_inst = \
                get_instances(timestamp, pm25_val,
                              pm10_val, city.official_name)
            if city_inst:
                update_database(city_inst, pm25_inst, pm10_inst)


def get_data(city_name):
    config = current_app.config
    endpoint = (config.get('AIR_QUALITY_API_URL')
                + city_name
                + '/?token='
                + config.get('AIR_QUALITY_API_TOKEN'))

    data = requests.get(endpoint).json()
    timestamp = get_nested(data, *config.get('TIMESTAMP_API_LOCATION'))
    pm25_val = get_nested(data, *config.get('PM25_API_LOCATION'))
    pm10_val = get_nested(data, *config.get('PM10_API_LOCATION'))
    return timestamp, pm25_val, pm10_val


def get_instances(timestamp, pm25_val, pm10_val, city_name):
    date = datetime.fromtimestamp(timestamp)
    pm25_inst = Pm25Record(value=pm25_val, date=date)
    pm10_inst = Pm10Record(value=pm10_val, date=date)
    city_inst = City.query.filter_by(name=city_name).first()
    return city_inst, pm25_inst, pm10_inst


def update_database(city_inst, pm25_inst, pm10_inst):
    if Pm10Record.query.filter_by(date=pm10_inst.date,
                                  city_id=city_inst.id).count() < 1:
        city_inst.pm10.append(pm10_inst)
    if Pm25Record.query.filter_by(date=pm25_inst.date,
                                  city_id=city_inst.id).count() < 1:
        city_inst.pm25.append(pm25_inst)
    db.session.commit()
