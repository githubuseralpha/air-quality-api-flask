from flask_scheduler.flask_scheduler import FlaskScheduler

scheduler = FlaskScheduler(timezone="Europe/Warsaw", hours=10)

from . import jobs
