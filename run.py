import os

from app import create_app, db
from app.models import City, Country

app = create_app(os.getenv('FLASK_CONFIG') or 'development')


@app.cli.command("populate")
def populate_database():
    db.drop_all()
    db.create_all()
    for city in app.config.get('TRACKED_CITIES'):
        country = (Country.query.filter(Country.name == city.country_name)
                   .first())
        if not country:
            country = Country(name=city.country_name)
        city = City(name=city.official_name)
        country.cities.append(city)
        db.session.add(country)
    db.session.commit()
